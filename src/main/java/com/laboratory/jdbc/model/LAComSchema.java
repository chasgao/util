/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.laboratory.jdbc.model;

import java.sql.*;
import java.io.*;
import java.util.Date;

/*
 * <p>ClassName: LAComSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2016-09-14
 */
public class LAComSchema   
{
	// @Field
	/** 代理机构 */
	private String AgentCom;
	/** 管理机构 */
	private String ManageCom;
	/** 地区类型 */
	private String AreaType;
	/** 渠道类型 */
	private String ChannelType;
	/** 上级代理机构 */
	private String UpAgentCom;
	/** 机构名称 */
	private String Name;
	/** 机构注册地址 */
	private String Address;
	/** 机构邮编 */
	private String ZipCode;
	/** 机构电话 */
	private String Phone;
	/** 机构传真 */
	private String Fax;
	/** Email */
	private String EMail;
	/** 网址 */
	private String WebAddress;
	/** 负责人 */
	private String LinkMan;
	/** 密码 */
	private String Password;
	/** 法人 */
	private String Corporation;
	/** 银行编码 */
	private String BankCode;
	/** 银行帐号 */
	private String BankAccNo;
	/** 行业分类 */
	private String BusinessType;
	/** 单位性质 */
	private String GrpNature;
	/** 中介机构类别 */
	private String ACType;
	/** 销售资格 */
	private String SellFlag;
	/** 操作员代码 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;
	/** 银行级别 */
	private String BankType;
	/** 是否统计网点合格率 */
	private String CalFlag;
	/** 工商执照编码 */
	private String BusiLicenseCode;
	/** 保险公司id */
	private String InsureID;
	/** 保险公司负责人 */
	private String InsurePrincipal;
	/** 主营业务 */
	private String ChiefBusiness;
	/** 营业地址 */
	private String BusiAddress;
	/** 签署人 */
	private String SubscribeMan;
	/** 签署人职务 */
	private String SubscribeManDuty;
	/** 许可证号码 */
	private String LicenseNo;
	/** 行政区划代码 */
	private String RegionalismCode;
	/** 上报代码 */
	private String AppAgentCom;
	/** 机构状态 */
	private String State;
	/** 相关说明 */
	private String Noti;
	/** 行业代码 */
	private String BusinessCode;
	/** 许可证登记日期 */
	private Date LicenseStartDate;
	/** 许可证截至日期 */
	private Date LicenseEndDate;
	/** 展业类型 */
	private String BranchType;
	/** 渠道 */
	private String BranchType2;
	/** 资产 */
	private double Assets;
	/** 营业收入 */
	private double Income;
	/** 营业利润 */
	private double Profits;
	/** 机构人数 */
	private int PersonnalSum;
	/** 合同编码 */
	private String ProtocalNo;
	/** 所属总行 */
	private String HeadOffice;
	/** 成立日期 */
	private Date FoundDate;
	/** 停业日期 */
	private Date EndDate;
	/** 分管代理保险业务负责人工号 */
	private String RespPersonCode;
	/** 分管代理保险业务负责人姓名 */
	private String RespPersonName;
	/** 销售人员工号 */
	private String SalePersonCode;
	/** 销售人员姓名 */
	private String SalePersonName;
	/** 保险代理从业人员资格证书号 */
	private String SaleQuafNo;
	/** 营业执照代码 */
	private String BusinessNo;
	/** 组织机构代码 */
	private String OrganizationNo;
	/** 营业执照有效起期 */
	private Date BusinessStartDate;
	/** 营业执照有效止期 */
	private Date BusinessEndDate;
	/** 组织机构代码有效起期 */
	private Date OrganizationStartDate;
	/** 组织机构代码有效止期 */
	private Date OrganizationEndDate;
	/** 组织机构代码（网点） */
	private String OrgaNo;
	/** 组织机构代码有效起期(网点) */
	private Date OrgaStartDate;
	/** 组织机构代码有效止期(网点) */
	private Date OrgaEndDate;
	/** 统一社会信用代码 */
	private String CommonCreditCode;
	/** 营业执照类型 */
	private String LicenseType;

	public static final int FIELDNUM = 69;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	public String getAgentCom() {
		return AgentCom;
	}

	public void setAgentCom(String agentCom) {
		AgentCom = agentCom;
	}

	public String getManageCom() {
		return ManageCom;
	}

	public void setManageCom(String manageCom) {
		ManageCom = manageCom;
	}

	public String getAreaType() {
		return AreaType;
	}

	public void setAreaType(String areaType) {
		AreaType = areaType;
	}

	public String getChannelType() {
		return ChannelType;
	}

	public void setChannelType(String channelType) {
		ChannelType = channelType;
	}

	public String getUpAgentCom() {
		return UpAgentCom;
	}

	public void setUpAgentCom(String upAgentCom) {
		UpAgentCom = upAgentCom;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getAddress() {
		return Address;
	}

	public void setAddress(String address) {
		Address = address;
	}

	public String getZipCode() {
		return ZipCode;
	}

	public void setZipCode(String zipCode) {
		ZipCode = zipCode;
	}

	public String getPhone() {
		return Phone;
	}

	public void setPhone(String phone) {
		Phone = phone;
	}

	public String getFax() {
		return Fax;
	}

	public void setFax(String fax) {
		Fax = fax;
	}

	public String getEMail() {
		return EMail;
	}

	public void setEMail(String eMail) {
		EMail = eMail;
	}

	public String getWebAddress() {
		return WebAddress;
	}

	public void setWebAddress(String webAddress) {
		WebAddress = webAddress;
	}

	public String getLinkMan() {
		return LinkMan;
	}

	public void setLinkMan(String linkMan) {
		LinkMan = linkMan;
	}

	public String getPassword() {
		return Password;
	}

	public void setPassword(String password) {
		Password = password;
	}

	public String getCorporation() {
		return Corporation;
	}

	public void setCorporation(String corporation) {
		Corporation = corporation;
	}

	public String getBankCode() {
		return BankCode;
	}

	public void setBankCode(String bankCode) {
		BankCode = bankCode;
	}

	public String getBankAccNo() {
		return BankAccNo;
	}

	public void setBankAccNo(String bankAccNo) {
		BankAccNo = bankAccNo;
	}

	public String getBusinessType() {
		return BusinessType;
	}

	public void setBusinessType(String businessType) {
		BusinessType = businessType;
	}

	public String getGrpNature() {
		return GrpNature;
	}

	public void setGrpNature(String grpNature) {
		GrpNature = grpNature;
	}

	public String getACType() {
		return ACType;
	}

	public void setACType(String aCType) {
		ACType = aCType;
	}

	public String getSellFlag() {
		return SellFlag;
	}

	public void setSellFlag(String sellFlag) {
		SellFlag = sellFlag;
	}

	public String getOperator() {
		return Operator;
	}

	public void setOperator(String operator) {
		Operator = operator;
	}

	public Date getMakeDate() {
		return MakeDate;
	}

	public void setMakeDate(Date makeDate) {
		MakeDate = makeDate;
	}

	public String getMakeTime() {
		return MakeTime;
	}

	public void setMakeTime(String makeTime) {
		MakeTime = makeTime;
	}

	public Date getModifyDate() {
		return ModifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		ModifyDate = modifyDate;
	}

	public String getModifyTime() {
		return ModifyTime;
	}

	public void setModifyTime(String modifyTime) {
		ModifyTime = modifyTime;
	}

	public String getBankType() {
		return BankType;
	}

	public void setBankType(String bankType) {
		BankType = bankType;
	}

	public String getCalFlag() {
		return CalFlag;
	}

	public void setCalFlag(String calFlag) {
		CalFlag = calFlag;
	}

	public String getBusiLicenseCode() {
		return BusiLicenseCode;
	}

	public void setBusiLicenseCode(String busiLicenseCode) {
		BusiLicenseCode = busiLicenseCode;
	}

	public String getInsureID() {
		return InsureID;
	}

	public void setInsureID(String insureID) {
		InsureID = insureID;
	}

	public String getInsurePrincipal() {
		return InsurePrincipal;
	}

	public void setInsurePrincipal(String insurePrincipal) {
		InsurePrincipal = insurePrincipal;
	}

	public String getChiefBusiness() {
		return ChiefBusiness;
	}

	public void setChiefBusiness(String chiefBusiness) {
		ChiefBusiness = chiefBusiness;
	}

	public String getBusiAddress() {
		return BusiAddress;
	}

	public void setBusiAddress(String busiAddress) {
		BusiAddress = busiAddress;
	}

	public String getSubscribeMan() {
		return SubscribeMan;
	}

	public void setSubscribeMan(String subscribeMan) {
		SubscribeMan = subscribeMan;
	}

	public String getSubscribeManDuty() {
		return SubscribeManDuty;
	}

	public void setSubscribeManDuty(String subscribeManDuty) {
		SubscribeManDuty = subscribeManDuty;
	}

	public String getLicenseNo() {
		return LicenseNo;
	}

	public void setLicenseNo(String licenseNo) {
		LicenseNo = licenseNo;
	}

	public String getRegionalismCode() {
		return RegionalismCode;
	}

	public void setRegionalismCode(String regionalismCode) {
		RegionalismCode = regionalismCode;
	}

	public String getAppAgentCom() {
		return AppAgentCom;
	}

	public void setAppAgentCom(String appAgentCom) {
		AppAgentCom = appAgentCom;
	}

	public String getState() {
		return State;
	}

	public void setState(String state) {
		State = state;
	}

	public String getNoti() {
		return Noti;
	}

	public void setNoti(String noti) {
		Noti = noti;
	}

	public String getBusinessCode() {
		return BusinessCode;
	}

	public void setBusinessCode(String businessCode) {
		BusinessCode = businessCode;
	}

	public Date getLicenseStartDate() {
		return LicenseStartDate;
	}

	public void setLicenseStartDate(Date licenseStartDate) {
		LicenseStartDate = licenseStartDate;
	}

	public Date getLicenseEndDate() {
		return LicenseEndDate;
	}

	public void setLicenseEndDate(Date licenseEndDate) {
		LicenseEndDate = licenseEndDate;
	}

	public String getBranchType() {
		return BranchType;
	}

	public void setBranchType(String branchType) {
		BranchType = branchType;
	}

	public String getBranchType2() {
		return BranchType2;
	}

	public void setBranchType2(String branchType2) {
		BranchType2 = branchType2;
	}

	public double getAssets() {
		return Assets;
	}

	public void setAssets(double assets) {
		Assets = assets;
	}

	public double getIncome() {
		return Income;
	}

	public void setIncome(double income) {
		Income = income;
	}

	public double getProfits() {
		return Profits;
	}

	public void setProfits(double profits) {
		Profits = profits;
	}

	public int getPersonnalSum() {
		return PersonnalSum;
	}

	public void setPersonnalSum(int personnalSum) {
		PersonnalSum = personnalSum;
	}

	public String getProtocalNo() {
		return ProtocalNo;
	}

	public void setProtocalNo(String protocalNo) {
		ProtocalNo = protocalNo;
	}

	public String getHeadOffice() {
		return HeadOffice;
	}

	public void setHeadOffice(String headOffice) {
		HeadOffice = headOffice;
	}

	public Date getFoundDate() {
		return FoundDate;
	}

	public void setFoundDate(Date foundDate) {
		FoundDate = foundDate;
	}

	public Date getEndDate() {
		return EndDate;
	}

	public void setEndDate(Date endDate) {
		EndDate = endDate;
	}

	public String getRespPersonCode() {
		return RespPersonCode;
	}

	public void setRespPersonCode(String respPersonCode) {
		RespPersonCode = respPersonCode;
	}

	public String getRespPersonName() {
		return RespPersonName;
	}

	public void setRespPersonName(String respPersonName) {
		RespPersonName = respPersonName;
	}

	public String getSalePersonCode() {
		return SalePersonCode;
	}

	public void setSalePersonCode(String salePersonCode) {
		SalePersonCode = salePersonCode;
	}

	public String getSalePersonName() {
		return SalePersonName;
	}

	public void setSalePersonName(String salePersonName) {
		SalePersonName = salePersonName;
	}

	public String getSaleQuafNo() {
		return SaleQuafNo;
	}

	public void setSaleQuafNo(String saleQuafNo) {
		SaleQuafNo = saleQuafNo;
	}

	public String getBusinessNo() {
		return BusinessNo;
	}

	public void setBusinessNo(String businessNo) {
		BusinessNo = businessNo;
	}

	public String getOrganizationNo() {
		return OrganizationNo;
	}

	public void setOrganizationNo(String organizationNo) {
		OrganizationNo = organizationNo;
	}

	public Date getBusinessStartDate() {
		return BusinessStartDate;
	}

	public void setBusinessStartDate(Date businessStartDate) {
		BusinessStartDate = businessStartDate;
	}

	public Date getBusinessEndDate() {
		return BusinessEndDate;
	}

	public void setBusinessEndDate(Date businessEndDate) {
		BusinessEndDate = businessEndDate;
	}

	public Date getOrganizationStartDate() {
		return OrganizationStartDate;
	}

	public void setOrganizationStartDate(Date organizationStartDate) {
		OrganizationStartDate = organizationStartDate;
	}

	public Date getOrganizationEndDate() {
		return OrganizationEndDate;
	}

	public void setOrganizationEndDate(Date organizationEndDate) {
		OrganizationEndDate = organizationEndDate;
	}

	public String getOrgaNo() {
		return OrgaNo;
	}

	public void setOrgaNo(String orgaNo) {
		OrgaNo = orgaNo;
	}

	public Date getOrgaStartDate() {
		return OrgaStartDate;
	}

	public void setOrgaStartDate(Date orgaStartDate) {
		OrgaStartDate = orgaStartDate;
	}

	public Date getOrgaEndDate() {
		return OrgaEndDate;
	}

	public void setOrgaEndDate(Date orgaEndDate) {
		OrgaEndDate = orgaEndDate;
	}

	public String getCommonCreditCode() {
		return CommonCreditCode;
	}

	public void setCommonCreditCode(String commonCreditCode) {
		CommonCreditCode = commonCreditCode;
	}

	public String getLicenseType() {
		return LicenseType;
	}

	public void setLicenseType(String licenseType) {
		LicenseType = licenseType;
	}

	public static String[] getPK() {
		return PK;
	}

	public static void setPK(String[] pK) {
		PK = pK;
	}

	public static int getFieldnum() {
		return FIELDNUM;
	}


 
}
