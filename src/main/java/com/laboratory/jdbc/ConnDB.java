package com.laboratory.jdbc;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.laboratory.io.KeyinTest;

/**
 * pls/ql
 * @author gaochao
 *
 */
public class ConnDB {

	
	
	public static void main(String[] args) {
//		new ConnDB().getConnection4Oracle();
//			new ConnDB().getConnection4MySQL();
		new ConnDB().query();
		
		
	}
	
	/**
	 * 连接oracle
	 * @return
	 */
	private  Connection getConnection4Oracle(){
		Connection con = null;// 创建一个数据库连接
		try {
			System.out.println("开始连接oracle");
			Class.forName("oracle.jdbc.driver.OracleDriver");// 加载Oracle驱动程序
//			String url="jdbc:oracle:thin:@10.5.9.62:1521:ORCL";//我本地数据库地址
//			String user="scott";
//			String password="tiger";
			String url="jdbc:oracle:thin:@10.1.104.35:1521:hxkf";
			String user="lis";
			String password="hxkf#lis";
			con=DriverManager.getConnection(url,user,password);
			System.out.println("连接oracle成功");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return con;
	}
	/**
	 * 连接mysql
	 * @return
	 */
	private  Connection getConnection4MySQL(){
		Connection con = null;// 创建一个数据库连接
		try {
			System.out.println("开始连接MySQL");
			Class.forName("com.mysql.jdbc.Driver");// 加载Oracle驱动程序
			String url="jdbc:mysql://127.0.0.1:3306/credit";
			String user="root";
			String password="a123";
			con=DriverManager.getConnection(url,user,password);
			System.out.println("连接MySQL成功");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return con;
	}	
	/**
	 * 
	 */
	public void query(){
		Connection con=getConnection4Oracle();
//		Connection con=getConnection4MySQL();
	    PreparedStatement pre = null;// 创建预编译语句对象，一般都是用这个而不用Statement
	    ResultSet result = null;// 创建一个结果集对象
	    FileWriter writer=null;
	    
//		String sql="select  * from lacom where  trim(agentcom) = ?";
		String sql = "select t1.comcode,t1.name, t2.comcode AS upcomcode ,t2.name AS upcomname, t1.comgrade from ldcom t1, ldcom t2 where t1.upcomcode= t2.comcode";
		try {
			writer= new FileWriter("D:\\!workspaceMyeclipse2015\\Test\\src\\outfile2.txt");
			System.out.println("开始查询");
//			pre=con.prepareStatement(sql);//使用预编译
			/**
			 * JDBC流式查询避免数据量过大导致OOM
			 * 只是prepareStatement时候改变了参数，并且设置了PreparedStatement的fetchsize为Integer.MIN_VALUE
			 */
			pre=con.prepareStatement(sql, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
//			pre.setFetchSize(Integer.MIN_VALUE);
			//sql参数赋值
//			pre.setString(1, "010707050003");
			result = pre.executeQuery();
			while(result.next()){
				String comcode=result.getString("comcode");
				String name=result.getString("name");
				String upcomcode=result.getString("upcomcode");
				String upcomname=result.getString("upcomname");
				String str = "comcode:"+comcode+"name:"+ name+"  upcomcode"+upcomcode+"upcomname"+upcomname+"\n";
				System.out.println(str);
				writer.write(str);
			}
			System.out.println("查询结束");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			//关闭资源。关闭顺序：最后使用的最先关闭。
			try {
				writer.close();
				if(result!=null)	result.close();
				if(pre != null)		pre.close();
				if (con != null)	con.close();
			} catch (Exception e) {
				e.printStackTrace();
				}
		}
		
	}

	public void outputFile()
	{
		
		
		
	}
	/**
	 * insert操作
	 * @param user
	 * @return
	 */
	public int insert(User user) {
//		Connection con=getConnection4Oracle();
		Connection con=getConnection4MySQL();
		int i= 0;
	    PreparedStatement pre = null;// 创建预编译语句对象，一般都是用这个而不用Statement
		//以后要是insert多条记录，在sql结尾加上多个小括号，sql也不要参数了。
		String sql = "insert into user (login_name ,name)  values (? ,? ,?)";
		try {
			System.out.println("开始insert");
			pre = con.prepareStatement(sql);
			pre.setString(1, user.getLoginName());
			pre.setString(2, user.getName());
//			i= pre.executeUpdate(sql);
			i= pre.executeUpdate();
			System.out.println("insert操作结束。共有 插入记录条数 "+ i);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (pre!= null) pre.close();
				if (con!= null) con.close();
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		return i;
	}
	
	/**
	 * update操作
	 * @param u
	 * @return
	 */
	public int update(User user){
//		Connection con=getConnection4Oracle();
		Connection con=getConnection4MySQL();
		int i= 0;
	    PreparedStatement pre = null;// 创建预编译语句对象，一般都是用这个而不用Statement
		String sql = "insert user set login_name = ? and name = ? where id = ?";
		try {
			System.out.println("开始update");
			pre = con.prepareStatement(sql);
			pre.setString(1, user.getLoginName());
			pre.setString(2, user.getName());
			pre.setInt(3, user.getId());
			i= pre.executeUpdate();
			System.out.println("update操作结束。共有更新记录条数 "+ i);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (pre!= null) pre.close();
				if (con!= null) con.close();
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		return i;
	}

	/**
	 * delete操作
	 * @param user
	 * @return
	 */
	private int delete(User user){
//		Connection con=getConnection4Oracle();
		Connection con=getConnection4MySQL();
		int i= 0;
	    PreparedStatement pre = null;// 创建预编译语句对象，一般都是用这个而不用Statement
		String sql = "delete from user  where id = ?";
		try {
			System.out.println("开始delete");
			pre = con.prepareStatement(sql);
			pre.setInt(1, user.getId());
			i= pre.executeUpdate();
			System.out.println("delete操作结束。共有删除记录条数 "+ i);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (pre!= null) pre.close();
				if (con!= null) con.close();
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		return i;
	}
			
	
	
	
}
