package com.laboratory.recurrence;

import java.util.Scanner;

/**
 * 递归方式计算一个整数 n的阶乘；0!的结果是1，规定
 * @author administator
 *
 */
public class factorialCalculate {

	public static void main(String[] args) {
//		System.out.println(factorial(25));//7034535277573963776
//		System.out.println(factorial(26));//-1569523520172457984,所以n>25以后，结果节不准确了。
		Scanner scanner=new Scanner(System.in);
		System.out.println("输入：");
		int n=scanner.nextInt();
		System.out.println(factorial(n));
	}
	/*
	 * 只能 计算25以内的小数字的结果，大数的阶乘有点麻烦
	 */
	public static long factorial(int n){
		long result=1;
		if(n<0) n=-n;
		if(n==1|| n==0){
			return 1;//递归的出口
		}
		result=n*factorial(n-1);//向递归出口靠拢
		
		return result;
	}
	
}
