package com.laboratory.SystemClass;

import java.io.IOException;

/**
 * Description:
 * <br/>Copyright (C), 2005-2008, Yeeku.H.Lee
 * <br/>This program is protected by copyright laws.
 * <br/>Program Name:
 * <br/>Date:
 * @author  Yeeku.H.Lee kongyeeku@163.com
 * @version  1.0
 */
public class TestExec
{
	public static void main(String[] args)throws Exception
	{
		TestExec te = new TestExec();
//		te.execNotepad();
		te.execKFGLPT();
	}
	public void execCalc(){
		Runtime rt = Runtime.getRuntime();
//		String [] cmdarray ={"notepad++.exe", "D:\\kaka.xml"};
		try {
			rt.exec("calc.exe");
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}	
	
	public void execNotepad(){
		Runtime rt = Runtime.getRuntime();
		// the specified command and arguments in a separate process. 
		String [] cmdarray ={"D:\\Program Files\\Notepad++\\notepad++.exe", "D:\\kaka.xml"};
		try {
			rt.exec(cmdarray);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public void execKFGLPT(){
		Runtime rt = Runtime.getRuntime();
		// the specified command and arguments in a separate process. 
		String [] cmdarray ={"D:\\文件4-21\\开发管理平台查询EXE(备用机).exe" };
//		String [] cmdarray ={"D:\\文件4-21\\开发管理平台查询EXE(备用机).exe", "103183"};

		try {
			rt.exec(cmdarray);
		} catch (IOException e) {
			e.printStackTrace();
		}
	
	}
	
	
	
}
