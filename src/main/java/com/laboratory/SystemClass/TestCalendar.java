package com.laboratory.SystemClass;

import java.util.*;
import static java.util.Calendar.*;

import java.text.SimpleDateFormat;

/**
 * Description: <br/>
 * Copyright (C), 2005-2008, Yeeku.H.Lee <br/>
 * This program is protected by copyright laws. <br/>
 * Program Name: <br/>
 * Date:
 * 
 * @author Yeeku.H.Lee kongyeeku@163.com
 * @version 1.0
 */
public class TestCalendar {
	public static void main(String[] args) {
		/**
		  SimpleDateFormat函数语法：
		  
		  G 年代标志符
		  y 年
		  M 月
		  d 日
		  h 时 在上午或下午 (1~12)
		  H 时 在一天中 (0~23)
		  m 分
		  s 秒
		  S 毫秒
		  E 星期
		  D 一年中的第几天
		  F 一月中第几个星期几
		  w 一年中第几个星期
		  W 一月中第几个星期
		  a 上午 / 下午 标记符 
		  k 时 在一天中 (1~24)
		  K 时 在上午或下午 (0~11)
		  z 时区
		 */
		Calendar c = Calendar.getInstance();
		System.out.println(c.getActualMaximum(Calendar.DAY_OF_MONTH));//一个月的最多天数
		// 取出年
		System.out.println(c.get(YEAR));
		// 取出月份;Calendar.MONTH月份的起始值是0不是1
		System.out.println(c.get(MONTH)+1);		
		// 取出日
		System.out.println(c.get(DATE));
		// 分别设置年、月、日、小时、分钟、秒
		c.set(2003, 10, 23, 12, 32, 23);// 2003-11-23 12:32:23
		SimpleDateFormat tSimpleDateFormat =  new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
		System.out.println(c.getTime());///格式不好看。。
		System.out.println(tSimpleDateFormat.format(c.getTime()));
		// 将Calendar的年前推1年
		c.add(YEAR, -1); // 2002-11-23 12:32:23
		System.out.println(tSimpleDateFormat.format(c.getTime()));
		// 将Calendar的月前推8个月
		c.roll(MONTH, -8); // 2002-03-23 12:32:23
		//c.add(DATE, 9);roll 与add相似，区别在于roll加上的value超过了该字段所能表达的最大范围后，也不会向上一个字段进位。
		
		System.out.println(tSimpleDateFormat.format(c.getTime()));

	}
}
