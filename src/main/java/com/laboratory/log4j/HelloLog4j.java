package com.laboratory.log4j;

import java.io.FileInputStream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.ConfigurationSource;
import org.apache.logging.log4j.core.config.Configurator;

public class HelloLog4j {
	private static Logger logger=LogManager.getLogger(HelloLog4j.class);
	
	public static void main(String[] args ) throws Exception{
		//到制定路径读取配置文件
//		PropertyConfigurator.configure("src/log4j/log4j.properties");
		
		ConfigurationSource tConfigurationSource =
				new ConfigurationSource(new FileInputStream("src/main/java/com/laboratory/log4j/log4j.properties"));
		
		Configurator.initialize(null, tConfigurationSource); 
		
		
		logger.info("info级别的日志信息。。。");
	}
	
	
}
