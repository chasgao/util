package com.laboratory.util;

import java.util.ArrayList;
import java.util.List;

/**
 * 数组相关的工具类
 */
public class ArrayUtil {

    /**
     * 获取一个double类型的数字的小数位有多长
     * @param dd
     * @return
     */
    public static int doueleBitCount(double dd){
        String temp = String.valueOf(dd);
        int i = temp.indexOf(".");
        if(i > -1){
            return temp.length()-i -1;
        }
        return 0;

    }

    public static Integer[] doubleBitCount(double[] arr){
        Integer[] len = new Integer[arr.length];
        for (int i = 0; i < arr.length; i++) {
            len[i] = doueleBitCount(arr[i]);
        }
        return len;
    }
    
	/**拷贝数组的两种方法：
	 * 1.使用循环遍历
	 * 2.使用System类的arrayCopy方法
	 * @param args
	 */	
    public void copy2AnotherArray(){

		int [] a={1,2,3,4};
		int [] b=new int[5];
		
		System.arraycopy(a, 0, b, 0, 4);
		for(int i : b){
			System.out.print(i+" ");
		}
		
		List<Integer> c=new ArrayList<Integer>();
		c.add(1);c.add(2);c.add(3);c.add(4);
		List<Integer> d=new ArrayList<Integer>();
		d.add(3);d.add(4);
		System.out.println();	
    }
}
