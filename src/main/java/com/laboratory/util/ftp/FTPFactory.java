package com.laboratory.util.ftp;

import com.laboratory.util.LaboratoryConfig;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;

/**
 * FTP工具类
 */
public class FTPFactory {


    //获取一个实例
    public static FTPUtil getInstance(String Name) throws IOException {

        String host = LaboratoryConfig.get(Name + ".host");
        if (host != null) {
            int port = Integer.parseInt(LaboratoryConfig.get(Name + ".port"));
            String username = LaboratoryConfig.get(Name + ".username");
            String password = LaboratoryConfig.get(Name + ".password");
            String remoteDir = LaboratoryConfig.get(Name + ".remoteDir");
            String localDir = LaboratoryConfig.get(Name + ".localDir");
            String Encoding = LaboratoryConfig.get(Name + ".Encoding");
            boolean passiveMode = new Boolean(LaboratoryConfig.get(Name + ".passiveMode")).booleanValue();
            FTPVo vo = new FTPVo(host, port, username, password, remoteDir, localDir, Encoding, passiveMode);
            return new FTPUtilImpl(vo);
        } else {
            throw new IOException("config error");
        }
    }
}
