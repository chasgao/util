package com.laboratory.exception;

public class TryCatchFinallyTest {

	public static void main(String[] args) {
		System.out.println(ReturnTest());;
		
	}
	
	public static int ReturnTest(){
		int a=0,b=0;
		try {
			b=a+1;
			System.out.println(b);
			int c=b / 2;
			return ++b;
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("catch run!");
			return ++b;
		}finally{
			System.out.println("finlly run!");
			return 10;
		}
	}

}
