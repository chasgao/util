/** 
 * Project Name:credit-server 
 * File Name:Test.java 
 * Package Name:com.credit.task 
 * Date:2017年4月13日下午5:17:09 
 * Copyright (c) 2017, 285116038@qq.com All Rights Reserved. 
 * 
*/  
  
package com.laboratory.string;  

import java.util.HashMap;
import java.util.Map;

/** 
 * 运行结果：Good and gbc
 * @version  V1.0 
 * @since    JDK 1.7 
 * @see       
 */
public class Test{
	String str=new String("Good");
	char[] ch={'a','b','c'};
	double a=1;
	public void change(String str,char [] ch){
		str="test ok";
		ch[0]='g';
	}
	public static void main(String[] args) {
		Test t=new Test();
		t.change(t.str, t.ch);
		System.out.println(t.str);
		System.out.println(t.ch[0]);
	}
	
	/**
	 * 从String.java源码中可以知道， string的实现是通过char[] 数组的形式，
	 * 之所以String对象不管怎么整都不会改变是因为， -- String对象是一个final对象，(不是主要原因)
	 * String类的主力成员字段value是final修饰的char[ ]数组  ，private final char value[];
	 * 而且String源码中存储字符的char[] 数组也是使用final修饰的，（属性的final修饰才是根本原因）
	 * 就是不变的对象，里面的属性也是不变的对象，所以不管你怎么弄他都是不变的，
	 * 这个就是String类型和其他基本类型不一样的地方，所以网上说各种原因的，看一下源码就知道为什么了。
	 * 一般引用类型的在传参时传入的是引用地址，实例能在方法中被修改，
	 * 但是String是特例，作为引用数据类型传参时，在方法中不会被修改，主要还是String源码中final修饰符的限制。
	 * String类型为什么做成不可变的，就是为了安全。
	 * https://www.zhihu.com/question/31345592
	 */
}
