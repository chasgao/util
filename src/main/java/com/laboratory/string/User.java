package com.laboratory.string;

import java.util.Date;

public class User {
	private int userId;
	private String userName;
	private String passWrod;
	private int credits;
	private String lastIp;
	private Date lastVisit;

	public User(int userId, String userName, String passWrod) {
		super();
		this.userId = userId;
		this.userName = userName;
		this.passWrod = passWrod;
	}

	@Override
	public String toString() {
		return "User [userId=" + userId + ", userName=" + userName
				+ ", passWrod=" + passWrod + ", credits=" + credits
				+ ", lastIp=" + lastIp + ", lastVisit=" + lastVisit + "]";
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassWrod() {
		return passWrod;
	}

	public void setPassWrod(String passWrod) {
		this.passWrod = passWrod;
	}

	public int getCredits() {
		return credits;
	}

	public void setCredits(int credits) {
		this.credits = credits;
	}

	public String getLastIp() {
		return lastIp;
	}

	public void setLastIp(String lastIp) {
		this.lastIp = lastIp;
	}

	public Date getLastVisit() {
		return lastVisit;
	}

	public void setLastVisit(Date lastVisit) {
		this.lastVisit = lastVisit;
	}

	public User() {
	}

	public User(int userId, String userName, String passWrod, int credits,
			String lastIp, Date lastVisit) {
		super();
		this.userId = userId;
		this.userName = userName;
		this.passWrod = passWrod;
		this.credits = credits;
		this.lastIp = lastIp;
		this.lastVisit = lastVisit;
	}

}
