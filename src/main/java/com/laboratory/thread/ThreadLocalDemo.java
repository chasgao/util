package com.laboratory.thread;

public class ThreadLocalDemo {

	public static void main(String[] args) {
		ThreadLocal<String> threadLocal = new ThreadLocal<>();
		threadLocal.set("mmma");
		System.out.println(threadLocal.get());
	}
}
