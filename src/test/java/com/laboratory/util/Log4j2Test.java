package com.laboratory.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

import junit.framework.TestCase;

public class Log4j2Test extends TestCase {

 	private static final Logger log=LogManager.getLogger(Log4j2Test.class);
	@Test
	public void test() {
		log.info("-==={}=={}==","11","22");
		log.debug("----%s--%s-------", "ddff", "fssf");//无效，没找到原因
		log.debug("--%1$-----%2$------------", "dd", "ff");

	}

}
